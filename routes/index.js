const express = require("express");
const router = express.Router();
const channel = require("./channel");
const user = require("./user");

router.use("/channels", channel);
router.use("/user", user);

module.exports = router;
