const express = require("express");
const router = express.Router();
const handler = require("../handler");

router.get("/", handler.user.getUser);
router.get("/:user_id", handler.user.userDetails);

module.exports = router;
