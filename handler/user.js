const external = require("../external");
const db = external.postgres;

module.exports = {
  getUser: (req, res) => {
    db.query("SELECT * FROM users", (err, data) => {
      return res.status(200).json(data.rows);
    });
  },
  userDetails: (req, res) => {
    const { user_id } = req.params;
    db.query(`SELECT * FROM users WHERE id = ${user_id}`, (err, data) => {
      return res.status(200).json(data.rows[0]);
    });
  },
};
